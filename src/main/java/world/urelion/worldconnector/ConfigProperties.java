package world.urelion.worldconnector;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * contains the fix property names and path in the {@link FileConfiguration}
 *
 * @since 1.0.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigProperties {
	/**
	 * name of the {@link ConfigurationSection},
	 * which defines all {@link World} pairs
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String WORLD_PAIRS    = "worldPairs";
	/**
	 * property name to define the lower {@link World}
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String LOWER_WORLD    = "lowerWorld";
	/**
	 * property name to define the upper {@link World}
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String UPPER_WORLD    = "upperWorld";
	/**
	 * property name to define the {@link Block} levels, which should overlap
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String BLOCK_OVERLAP  = "blockOverlap";
	/**
	 * property name to define the {@link Block} levels,
	 * in which the {@link Player}s and {@link Entity}s
	 * should be teleported to the other world
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String TELEPORT_RANGE = "teleportRange";
	/**
	 * name of {@link WorldPair}, which shows an example and will be ignored.
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String IGNORE_EXAMPLE = "ignoreExample";
}
