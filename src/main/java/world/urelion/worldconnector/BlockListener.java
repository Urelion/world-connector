package world.urelion.worldconnector;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * defines a {@link Listener}, which reacts on every {@link BlockEvent}s
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Singleton(style = Style.HOLDER)
public class BlockListener
implements Listener {
	/**
	 * the {@link Singleton} instance of {@link BlockListener}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final @NotNull BlockListener INSTANCE = new BlockListener();

	/**
	 * update a corresponding {@link Block} as a copy of the given one
	 *
	 * @param block the {@link Block} to copy the data from
	 *              and get the corresponding {@link Block} of
	 *
	 * @return a {@link Set} of all corresponding {@link Block}s
	 *
	 * @since 1.0.0
	 */
	@NotNull
	private Set<Block> updateBlock(
		final @NotNull Block block
	) {
		BlockListener.log.trace("Define set of corresponding blocks.");
		final Set<Block> otherBlocks = new HashSet<>();

		BlockListener.log.trace("Get plugin instance.");
		final WorldConnectorPlugin worldConnectorPlugin =
			WorldConnectorPlugin.getINSTANCE();
		BlockListener.log.trace("Check if plugin is initialized.");
		if (worldConnectorPlugin == null) {
			BlockListener.log.error(
				"Plugin not successfully initialized! Stop event execution."
			);
			return otherBlocks;
		}

		BlockListener.log.trace("Iterate over all world pairs.");
		for (final WorldPair worldPair : worldConnectorPlugin.getWorldPairs()) {
			BlockListener.log.trace(
				"Check if block of event is covered by world pair."
			);
			if (!(worldPair.overlaps(block.getLocation()))) {
				BlockListener.log.trace("Block is not covered by world pair.");
				continue;
			}

			BlockListener.log.trace(
				"Get corresponding block of the events block."
			);
			final Block otherBlock = worldPair.overlappedBlock(block);
			BlockListener.log.trace(
				"Check if a corresponding block was found."
			);
			if (otherBlock == null) {
				BlockListener.log.trace("No corresponding block was found.");
				continue;
			}

			BlockListener.log.trace("Add corresponding block to result set.");
			otherBlocks.add(otherBlock);

			BlockListener.log.trace("Get data of block.");
			final BlockData blockData = block.getBlockData();
			BlockListener.log.trace(
				"Check if target block is already in the right state."
			);
			if (BlockListener.blockDataEquals(
				otherBlock.getBlockData(),
				blockData
			)) {
				BlockListener.log.debug(
					"Data of target block are already in the right state."
				);
				continue;
			}

			BlockListener.log.trace(
				"Copy block status to corresponding block."
			);
			otherBlock.getWorld().setBlockData(
				otherBlock.getLocation(),
				blockData
			);
		}

		BlockListener.log.trace("Return set of corresponding blocks.");
		return otherBlocks;
	}

	/**
	 * process any {@link BlockEvent}
	 * and copy the {@link BlockData} to the corresponding block.
	 *
	 * @param event the {@link BlockEvent} to process
	 *
	 * @since 1.0.0
	 */
	private void onBlockUpdate(
		final BlockEvent event
	) {
		BlockListener.log.trace("Update block on event.");
		this.updateBlock(event.getBlock());
	}

	/**
	 * checks if multiple {@link BlockData} are all identical
	 *
	 * @param blockData the {@link BlockData} to check of equality
	 *
	 * @return {@code true} if and only if
	 *		   all given {@link BlockData} are identical to each other
	 *
	 * @since 1.0.0
	 */
	public static boolean blockDataEquals(
		final @NotNull BlockData... blockData
	) {
		BlockListener.log.trace("Iterate over all given BlockData.");
		for (final BlockData dataA : blockData) {
			BlockListener.log.trace("Iterate over all given BlockData.");
			for (final BlockData dataB : blockData) {
				BlockListener.log.trace(
					"Check if both BlockData are the same object."
				);
				if (dataA == dataB) {
					BlockListener.log.trace(
						"Both BlockData are the same object. Skip iteration."
					);
					continue;
				}
				BlockListener.log.trace(
					"Check if BlockData a matches BlockData b."
				);
				if (!(dataA.matches(dataB))) {
					BlockListener.log.debug(
						"BlockData " + dataA +
						" doesn't match BlockData " + dataB + ". Return false."
					);
					return false;
				}
			}
		}

		BlockListener.log.trace("No difference found. Return true.");
		return true;
	}

	/**
	 * process a {@link BlockBreakEvent}
	 *
	 * @param event the {@link BlockBreakEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(
		final BlockBreakEvent event
	) {
		BlockListener.log.trace("Iterate over all corresponding blocks.");
		for (final Block block : this.updateBlock(event.getBlock())) {
			BlockListener.log.trace("Set corresponding block to AIR.");
			block.setType(Material.AIR);
		}
	}

	/**
	 * process a {@link BlockPlaceEvent}
	 *
	 * @param event the {@link BlockPlaceEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockPlace(
		final BlockPlaceEvent event
	) {
		BlockListener.log.trace("Call block update on block placement.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockBurnEvent}
	 *
	 * @param event the {@link BlockBurnEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBurn(
		final BlockBurnEvent event
	) {
		BlockListener.log.trace("Call block update on block burn.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockDamageEvent}
	 *
	 * @param event the {@link BlockDamageEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockDamage(
		final BlockDamageEvent event
	) {
		BlockListener.log.trace("Call block update on block damage.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockExplodeEvent}
	 *
	 * @param event the {@link BlockExplodeEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockExplode(
		final BlockExplodeEvent event
	) {
		BlockListener.log.trace("Call block update on block explosion.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockFadeEvent}
	 *
	 * @param event the {@link BlockFadeEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockFade(
		final BlockFadeEvent event
	) {
		BlockListener.log.trace("Call block update on block fade.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockFertilizeEvent}
	 *
	 * @param event the {@link BlockFertilizeEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockFertilize(
		final BlockFertilizeEvent event
	) {
		BlockListener.log.trace("Call block update on block fertilize.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockGrowEvent}
	 *
	 * @param event the {@link BlockGrowEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockGrow(
		final BlockGrowEvent event
	) {
		BlockListener.log.trace("Call block update on block grow.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockIgniteEvent}
	 *
	 * @param event the {@link BlockIgniteEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockIgnite(
		final BlockIgniteEvent event
	) {
		BlockListener.log.trace("Call block update on block ignition.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link BlockRedstoneEvent}
	 *
	 * @param event the {@link BlockRedstoneEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockRedstone(
		final BlockRedstoneEvent event
	) {
		BlockListener.log.trace(
			"Call block update on redstone current change."
		);
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link CauldronLevelChangeEvent}
	 *
	 * @param event the {@link CauldronLevelChangeEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onCauldronLevelChange(
		final CauldronLevelChangeEvent event
	) {
		BlockListener.log.trace("Call block update on cauldron level change.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link FluidLevelChangeEvent}
	 *
	 * @param event the {@link FluidLevelChangeEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onFluidLevelChange(
		final FluidLevelChangeEvent event
	) {
		BlockListener.log.trace("Call block update on fluid level change.");
		this.onBlockUpdate(event);
	}

	/**
	 * process a {@link SignChangeEvent}
	 *
	 * @param event the {@link SignChangeEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onSignChange(
		final SignChangeEvent event
	) {
		BlockListener.log.trace("Call block update on sign change.");
		this.onBlockUpdate(event);
	}
}
