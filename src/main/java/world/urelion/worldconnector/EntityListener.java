package world.urelion.worldconnector;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

/**
 * defines a {@link Listener} to teleport {@link Player}s and {@link Entity}s
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Singleton(style = Style.HOLDER)
public class EntityListener
implements Listener {
	/**
	 * the {@link Singleton} instance of {@link EntityListener}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final @NotNull EntityListener INSTANCE =
		new EntityListener();

	/**
	 * process the movement of a {@link Player}
	 *
	 * @param event the {@link PlayerMoveEvent} to handle
	 *
	 * @since 1.0.0
	 */
	@EventHandler
	public void onPlayerMove(
		final PlayerMoveEvent event
	) {
		EntityListener.log.trace("Get plugin instance.");
		WorldConnectorPlugin worldConnectorPlugin =
			WorldConnectorPlugin.getINSTANCE();
		EntityListener.log.trace("Check if plugin is initialized.");
		if (worldConnectorPlugin == null) {
			EntityListener.log.error(
				"Plugin is not fully loaded! End event execution."
			);
			return;
		}
		EntityListener.log.trace("Get player of event.");
		Player player = event.getPlayer();
		EntityListener.log.trace("Get current location of player.");
		Location currentLoc = player.getLocation();

		EntityListener.log.trace("Iterate over all known world pairs.");
		for (WorldPair worldPair : worldConnectorPlugin.getWorldPairs()) {
			EntityListener.log.trace("Check if player is in teleport range.");
			if (worldPair.isInTeleportRange(currentLoc)) {
				EntityListener.log.trace(
					"Get target location for player teleport."
				);
				Location targetLoc = worldPair.overlappedLocation(currentLoc);
				EntityListener.log.trace("Check if target location exists.");
				if (targetLoc == null) {
					EntityListener.log.error(
						"Target location doesn't exist! End event execution."
					);
					continue;
				}

				EntityListener.log.trace("Copy yaw from current location.");
				targetLoc.setYaw(currentLoc.getYaw());
				EntityListener.log.trace("Copy pitch from current location.");
				targetLoc.setPitch(currentLoc.getPitch());
				EntityListener.log.trace("Get current velocity.");
				Vector velocity = player.getVelocity();
				EntityListener.log.debug(
					"Teleport player to corresponding location."
				);
				player.teleport(targetLoc);
				EntityListener.log.trace("Reset the velocity.");
				player.setVelocity(velocity);

				EntityListener.log.trace("End event execution.");
				return;
			}
		}
	}
}
